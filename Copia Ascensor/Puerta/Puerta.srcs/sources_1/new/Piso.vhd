----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.01.2020 01:18:52
-- Design Name: 
-- Module Name: Piso - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Mod_piso is
   port( 
 	 clk: in std_logic;
 	 Rst: in std_logic;
 	 suma: in std_logic;
 	 Motor: in std_logic_vector(0 to 1);
 	 Piso: out std_logic_vector(0 to 1)
 	 );
end Mod_piso;
 
architecture Behavioral of Mod_piso is
   signal piso_s: std_logic_vector(0 to 1);
begin 
 process (clk, rst)
  begin
      if Rst='0' then
         piso_s <= "00" ;
      elsif(rising_edge(clk)) then
           if suma = '1' then
              if motor="01" then
                piso_s <= piso_s + 1;
              elsif motor="10" then
                piso_s <= piso_s - 1 ;
              else 
                piso_s<=piso_s;
              end if;
           end if;
      end if;
   end process;
   piso <= piso_s; --signed(std_logic_vector(piso_s));
                
 
end Behavioral;