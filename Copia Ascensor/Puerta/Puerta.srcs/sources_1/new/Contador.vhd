----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.01.2020 04:17:05
-- Design Name: 
-- Module Name: Contador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


entity contador is
Port (
    clk : in std_logic;
    Rst: in std_logic;
    cnt_out : out std_logic_vector(2 downto 0);
    suma: out std_logic
    );
end contador;

architecture Behavioral of contador is
    signal puerta_out : unsigned(cnt_out 'range);
begin
    process(Rst, clk)
    begin
        if Rst= '0' then 
            puerta_out <= (others => '0');
        elsif clk' event and clk = '1' then
                puerta_out <= puerta_out + 1;
            end if;
    end process;
   cnt_out <= std_logic_vector(puerta_out); 
   suma <= '1' when puerta_out = 7 else -- Esperamos al cierre de puertas .
                    '0';
   
  end Behavioral;