----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.01.2020 04:26:21
-- Design Name: 
-- Module Name: Ascensor - Structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

Entity Ascensor is
    generic ( fmaq : positive := 1000;
              fpiso: positive := 1;
              fluz : positive := 2
            );
    Port ( Clk,Rst      : in  STD_LOGIC;
           Motor        : in  STD_LOGIC_VECTOR (1 downto 0);
           Puerta_ON    : in  std_logic;
           Fin_puerta   : out std_logic;
           Piso         : out STD_LOGIC_VECTOR (1 downto 0);
           Led_Motor    : out STD_LOGIC_VECTOR (2 downto 0);
           LEDs_serie   : out std_logic_vector (15 downto 0);
           Display_piso : out STD_LOGIC_VECTOR (6 downto 0);
           Digctrl      : out STD_LOGIC_VECTOR (7 downto 0)
           );
end Ascensor;

architecture Structural of Ascensor is

    component LedMotor is
  Port (
        motor: in std_logic_vector(1 downto 0);
        Led_Motor: out std_logic_vector(2 downto 0)
         );
end component LedMotor;

    component contador is
    Port (
        clk : in std_logic;
        Rst: in std_logic;
        cnt_out : out std_logic_vector(2 downto 0);
        suma: out std_logic
        );
    end component contador;

   component Mod_piso is
      port( 
         clk: in std_logic;
         Rst: in std_logic;
         Motor: in std_logic_vector(0 to 1);
         suma: in std_logic;
         Piso: out std_logic_vector(0 to 1)
         );
   end component;
   
   signal suma: std_logic;
   signal s_piso:std_logic_vector(0 to 1);
   
   COMPONENT clk_divider
   GENERIC (fin: positive;
            fout: positive);
   PORT(
        clk : IN  std_logic;
        reset_n : IN  std_logic;
        clk_out : OUT  std_logic
       );
   END COMPONENT;
   signal clk_out:std_logic;
   
   COMPONENT decodificador IS

    PORT (
    sw_piso : IN std_logic_vector(1 DOWNTO 0);
    led : OUT std_logic_vector(6 DOWNTO 0)
    );
   END COMPONENT;
   
   COMPONENT puerta is
       Generic (fmaq:integer ;
                fluz:integer 
               );
       Port ( 
           puerta_ON : in std_logic;
           clk : in std_logic;
           Rst: in std_logic;
           Fin_puerta : out std_logic;
           puertas_total : out std_logic_vector(15 downto 0)
              );
   END COMPONENT;
   signal s_puerta:std_logic_vector(15 downto 0);
   
begin

    LedM : LedMotor
                    port map ( motor        => motor,
                               Led_motor =>led_motor
                               );

   TiempoP : clk_divider 
            GENERIC MAP(  fin=> fmaq,
                          fout=> fpiso
                        )
                        
            PORT MAP (  clk     => clk,
                        reset_n => rst,
                        clk_out => clk_out
                     );

    ContadorP : contador
                    port map(clk =>clk_out,
                             rst=>rst,
                             suma=>suma
                            );
   PisoP : Mod_piso
                    port map(   motor => motor, 
                                clk => clk_out,
                                Rst => Rst,  
                                piso => s_piso, 
                                suma => suma
                            );
                            
    DecodificadorP :  Decodificador
                    PORT MAP ( sw_piso         => s_piso,
                               Led => Display_piso      
                              );

    PuertaP : puerta
      GENERIC MAP(fmaq=>fmaq,
                  fluz=>fluz
                  )
      PORT MAP(puerta_ON => puerta_on, 
               clk => clk,
               Rst => Rst, 
               Fin_puerta => Fin_puerta, 
               puertas_total => s_puerta
               );
    Leds_Serie<=s_puerta;                      
    piso<=s_piso;
    digctrl<=NOT"00000001";
end Structural;
