----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.01.2020 14:06:54
-- Design Name: 
-- Module Name: TOP - Structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Top is
 generic (
           fin   : positive := 100000000;--frec de la placa 100kHz
           fout  : positive := 1000;--frec de Top 1kHz
           fpiso : positive := 2;--frecuencia del contador de cambio de piso 2Hz
           fluz  : positive := 2;--Frecuencia de encendido de las luces de la puerta 2Hz
           NPISOS: positive := 4
         );
  Port (
          clk_in         : in std_logic;
          rst            : in std_logic;
          boton_int      : in std_logic_vector(NPISOS-1 downto 0);
          boton_ext      : in std_logic_vector(NPISOS-1 downto 0);
          led_motor      : out std_logic_vector(2 downto 0);
          LED            : out std_logic_vector (15 downto 0);
          Display_piso   : out STD_LOGIC_VECTOR (6 downto 0);
          digctrl        : out STD_LOGIC_VECTOR (7 downto 0)
         );
end Top;

architecture Structural of Top is

    component clk_divider is
        generic (fin: positive;
                 fout: positive);
        Port ( clk : in  STD_LOGIC;
               reset_n : in  STD_LOGIC;
               clk_out : out  STD_LOGIC);
    end component;
    signal clk_out:std_logic;
    
    component  M_estados is
     Generic( num_pisos:natural:=4);
     
     Port (clk,reset    : in STD_LOGIC;
           boton_int    : in STD_LOGIC_VECTOR(num_pisos-1 downto 0);
           boton_ext    : in STD_LOGIC_VECTOR(num_pisos-1 downto 0);
           piso         : out STD_LOGIC_VECTOR(1 downto 0);
           motor        : out STD_LOGIC_VECTOR(1 downto 0);
           puerta       : out STD_LOGIC );
end component M_estados;

    
    signal fin_puerta     : std_logic;
    signal piso           : std_logic_vector(1 downto 0);
    signal puerta         : std_logic;
    signal motor          : std_logic_vector(1 downto 0);
    signal LEDs_botones   : std_logic_vector (15 downto 0);
    
    Component Ascensor is
    generic ( fmaq : positive := 1000;
              fpiso: positive := 1;
              fluz : positive := 2
            );
    Port ( Clk,Rst      : in  STD_LOGIC;
           Motor        : in  STD_LOGIC_VECTOR (1 downto 0);
           Puerta_ON    : in  std_logic;
           Fin_puerta   : out std_logic;
           Piso         : out STD_LOGIC_VECTOR (1 downto 0);
           Led_Motor    : out STD_LOGIC_VECTOR (2 downto 0);
           LEDs_serie   : out std_logic_vector (15 downto 0);
           Display_piso : out STD_LOGIC_VECTOR (6 downto 0);
           Digctrl      : out STD_LOGIC_VECTOR (7 downto 0)
           );
end component Ascensor;

begin

Hz1000Hz : clk_divider
       generic map (fin => fin,
                    fout => fout
                    )
       port map    (clk =>clk_in,
                    reset_n=>rst,
                    clk_out=>clk_out
                    );
    Control_Ascensor:
     M_estados 
        Generic Map(num_pisos=>NPISOS)
        Port Map(clk=>clk_out,
                 reset=>rst,
                 boton_int=>boton_int,
                 boton_ext=>boton_ext,
                 piso=>piso,
                 motor=>motor,
                 puerta=>puerta
         );
                
    AscensorP:
    Ascensor   
         Generic Map( fmaq=>fout,
                      fpiso=>fpiso,
                      fluz=>fluz
                     )
         Port Map(clk=>clk_out,
                  rst=>rst,
                  fin_puerta=>fin_puerta,
                  piso=>piso,
                  motor=>motor,
                  puerta_on=>puerta,
                  Led_motor=>Led_motor,
                  LEDs_serie=>led,
                  Display_piso=>display_piso,
                  Digctrl=>digctrl                  
                 );


end Structural;
