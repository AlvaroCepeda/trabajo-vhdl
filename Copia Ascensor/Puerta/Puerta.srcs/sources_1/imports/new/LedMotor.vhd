----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.11.2019 02:11:20
-- Design Name: 
-- Module Name: LedMotor - DataFlow
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LedMotor is
  Port (
        motor: in std_logic_vector(1 downto 0);
        Led_Motor: out std_logic_vector(2 downto 0)
         );
end LedMotor;

architecture DataFlow of LedMotor is

begin

with motor select
    Led_Motor <= "100" when "00",-- Rojo si esta parado
                 "010" when "01",--Verde cuando sube
                 "001" when "10",--Azul cuando baja
                 "000" when others ;--Sin encender si pasa algo

end DataFlow;
