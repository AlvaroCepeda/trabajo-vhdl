----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.11.2019 23:22:57
-- Design Name: 
-- Module Name: Decodificador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

ENTITY decodificador IS

PORT (
sw_piso : IN std_logic_vector(1 DOWNTO 0);
led : OUT std_logic_vector(6 DOWNTO 0)
);

END ENTITY decodificador;

ARCHITECTURE dataflow OF decodificador IS

BEGIN

WITH sw_piso SELECT

led <= "0000001" WHEN "00",
"1001111" WHEN "01",
"0010010" WHEN "10",
"0000110" WHEN "11",
"0000001" WHEN others;

END ARCHITECTURE dataflow;

