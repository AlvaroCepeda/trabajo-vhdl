----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.11.2019 01:23:48
-- Design Name: 
-- Module Name: M_estados - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity M_estados is
     Generic( num_pisos:natural:=4);
     
     Port (clk,reset    : in STD_LOGIC;
           boton_int    : in STD_LOGIC_VECTOR(num_pisos-1 downto 0);
           boton_ext    : in STD_LOGIC_VECTOR(num_pisos-1 downto 0);
           piso         : out STD_LOGIC_VECTOR(1 downto 0);
           motor        : out STD_LOGIC_VECTOR(1 downto 0);
           puerta       : out STD_LOGIC );
end M_estados;

architecture Behavioral of M_estados is

type estados is ( S0, S1, S2, S3, S4 );
signal estado, estado_siguiente: estados;
signal piso_int:std_logic_vector(1 downto 0);
signal piso_ext:std_logic_vector(1 downto 0);
signal piso_actual:std_logic_vector(1 downto 0) := "00";
signal signal_motor: std_logic_vector (1 downto 0);

begin

with boton_int select
piso_int<="00" when "0001",
      "01" when "0010",
      "10" when "0100",
      "11" when "1000",
      "00" when "0000",--Esto seria para la primera vez que esta abajo 
      "ZZ" when others;
with boton_ext select
piso_ext<="00" when "0001",
      "01" when "0010",
      "10" when "0100",
      "11" when "1000",
      "00" when "0000",--Esto seria para la primera vez que esta abajo
      "ZZ" when others;
    
Sincronizar_estados: process(clk,reset)
    begin
        if reset ='0' then
            estado <= S0;
        elsif clk'event and clk='1' then
            estado <= estado_siguiente;
        end if;
    end process sincronizar_estados ;

Logicadelestadosiguiente: process( estado, piso_int , piso_ext )
    begin 
        --estado_siguiente <= estado;
        case estado is
            when S0 => 
                if (piso_actual<piso_int)or(piso_actual<piso_ext) then
                     estado_siguiente <= S1;
                else
                     estado_siguiente <= S0;
                end if;
            when S1 =>
                estado_siguiente <= S2;
            when S2 =>
                if (piso_actual<piso_int)or(piso_actual<piso_ext) then
                     estado_siguiente <= S1;
                elsif (piso_actual>piso_int)and(piso_actual>piso_ext) then
                     estado_siguiente <= S3;
                else 
                    estado_siguiente <= S2;
                end if;
            when S3 =>
                estado_siguiente <= S4;
            when S4 =>
                if (piso_actual<piso_int)or(piso_actual<piso_ext) then
                     estado_siguiente <= S1;
                elsif (piso_actual>piso_int)and(piso_actual>piso_ext) then
                     estado_siguiente <= S3;
                else 
                    estado_siguiente <= S4;
                end if;
        end case;
    end process Logicadelestadosiguiente;    

Asignacion_salidas: process( estado , piso_ext , piso_int )
     begin
        -- Valores por defecto de las salidas
        --motor  <= "00";
        --puerta <='1';  
        case estado is
            when S0 => 
                motor  <= "00";
                puerta <= '1';
            when S1 =>
                puerta <= '0';
                motor  <= "01";--subir
                if (boton_int="0001" or boton_ext="0001") then 
                piso_actual <= "00";
                elsif (piso_ext>piso_int) then
                piso_actual <= piso_ext;
                else
                piso_actual <= piso_int;
                end if;   
            when S2 =>
                puerta <= '1';
                motor  <= "00";--parado
            when S3 =>
                motor  <= "10";
                puerta <= '0';--bajar
                if (boton_int="0001" or boton_ext="0001") then 
                piso_actual <= "00";
                elsif (piso_ext>piso_int) then
                piso_actual <= piso_ext;
                else
                piso_actual <= piso_int;
                end if;  
                
            when S4 =>
                motor  <= "00";--parado
                puerta <= '1';

        end case;
        piso <= piso_actual;
    end process Asignacion_salidas;   
    
end Behavioral;
