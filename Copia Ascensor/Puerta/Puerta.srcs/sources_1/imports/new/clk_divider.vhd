
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_divider is
    generic (
             fin: positive;
             fout: positive);
    Port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           clk_out : out  STD_LOGIC);
end clk_divider;

architecture Behavioral of clk_divider is
signal clk_sig : std_logic;

begin
   process(reset_n,clk)
     variable cnt : integer;
     begin
       if (reset_n='0') then
         clk_sig<='0';
         cnt:=0;
       elsif rising_edge(clk) then
         if (cnt=fin/(2*fout)-1) then 
            clk_sig<=NOT(clk_sig);
            cnt:=0;
         else
           cnt:=cnt+1;
         end if;
       end if;
     end process;

   clk_out <= clk_sig;

end Behavioral;

