
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;



entity puertacomponent is
Port (
    puerta_ON : in std_logic;
    clk : in std_logic;
    Rst: in std_logic;
    Fin_puerta : out std_logic;
    puertas : out std_logic_vector(2 downto 0)
    );
end puertacomponent;

architecture Behavioral of puertacomponent is
    signal apertura: std_logic:='1';
    signal puerta_s : unsigned(puertas'high downto 0);
begin
   process(Rst, clk)
    begin
        if Rst= '0' then 
            puerta_s <= (others => '0');

        elsif clk' event and clk = '1' then
            Fin_puerta<='0';
            if puerta_ON = '1' then
               if apertura= '1' then
                  puerta_s <= puerta_s + 1;
                  if puerta_s=6 then
                    apertura<='0';
                  end if;
               elsif apertura = '0' then
                  puerta_s <= puerta_s - 1;
                  if puerta_s=1 then
                    Fin_puerta<='1';
                    apertura<='1';
                  end if;
            end if;
        end if;
      end if;
    end process;

   
                 
    puertas <= std_logic_vector(puerta_s);
end Behavioral;
