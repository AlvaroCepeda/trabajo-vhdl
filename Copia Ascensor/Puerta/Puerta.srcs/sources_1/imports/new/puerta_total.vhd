----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Claudiu
-- 
-- Create Date: 16.11.2019 19:43:56
-- Design Name: 
-- Module Name: puerta - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity puerta is
Generic (fmaq:integer :=1000;
         fluz:integer :=2
        );
Port ( 
    puerta_ON : in std_logic;
    clk : in std_logic;
    Rst: in std_logic;
    Fin_puerta : out std_logic;
    puertas_total : out std_logic_vector(15 downto 0)
       );
end puerta;

architecture structural of puerta is
signal s_puertas: std_logic_vector(2 downto 0);
signal s_salida: std_logic_vector(15 downto 0);

COMPONENT clk_divider
GENERIC (fin: positive;
        fout: positive);
PORT(
    clk : IN  std_logic;
    reset_n : IN  std_logic;
    clk_out : OUT  std_logic
   );
END COMPONENT;
signal clk_out:std_logic;

COMPONENT puertacomponent
PORT (
puerta_ON : in std_logic;
clk : in std_logic;
Rst: in std_logic;
Fin_puerta : out std_logic;
puertas : out std_logic_vector(2 downto 0)
);
END COMPONENT;

COMPONENT cerrar_puerta 
PORT (
cierre_de_puertas: in std_logic_vector(2 downto 0);
out_decod: out std_logic_vector(15 downto 0)
);
END COMPONENT;

begin
timer_luces: clk_divider 
 GENERIC MAP(  fin=> fmaq,
               fout=> fluz
             )
             
 PORT MAP (  clk     => clk,
             reset_n => rst,
             clk_out => clk_out
          );
inst_puertacomponent: puertacomponent 
PORT MAP (
puerta_ON => puerta_ON,
clk  => clk_out,
Rst => Rst,
Fin_puerta => Fin_puerta,
puertas => s_puertas
);

inst_decod_puerta: cerrar_puerta
PORT MAP (
out_decod => s_salida,
cierre_de_puertas => s_puertas
);

puertas_total <= s_salida;

end structural;
