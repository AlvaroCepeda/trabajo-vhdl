
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity cerrar_puerta is
Port (
       cierre_de_puertas: in std_logic_vector(2 downto 0);
       out_decod: out std_logic_vector(15 downto 0)
      );
end cerrar_puerta;

architecture dataflow of cerrar_puerta is

begin
with cierre_de_puertas select 
out_decod <= "0000000000000000" when "111",
             "1110000000000000" when "110",
             "1111100000000000" when "101",
             "1111111000000000" when "100",
             "1111111110000000" when "011",
             "1111111111100000" when "010",
             "1111111111111000" when "001",
             "1111111111111110" when "000",
             "0000000000000000" when others;
end dataflow;
