----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.01.2020 04:35:16
-- Design Name: 
-- Module Name: Ascensor_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ascensor_tb is
--  Port ( );
end Ascensor_tb;

architecture Behavioral of Ascensor_tb is

    Component Ascensor is
            generic (
                  fmaq : positive := 1000;
                  fpiso: positive := 1;
                  fluz : positive := 2
                );
            Port ( Clk,Rst      : in  STD_LOGIC;
                   Motor        : in  STD_LOGIC_VECTOR (1 downto 0);
                   Puerta_ON    : in  std_logic;
                   Fin_puerta   : out std_logic;
                   Piso         : out STD_LOGIC_VECTOR (1 downto 0);
                   Led_Motor    : out STD_LOGIC_VECTOR (2 downto 0);
                   LEDs_serie   : out std_logic_vector (15 downto 0);
                   Display_piso : out STD_LOGIC_VECTOR (6 downto 0);
                   Digctrl      : out STD_LOGIC_VECTOR (7 downto 0)
                   );
    end Component Ascensor;
    
    signal clk,rst      : std_logic;
    signal LED_Tricolor : std_logic_vector(2 downto 0);
    signal LEDs_serie   : std_logic_vector (15 downto 0);
    signal Display_piso : STD_LOGIC_VECTOR (6 downto 0);
    signal Digctrl      : STD_LOGIC_VECTOR (7 downto 0);
    signal Fin_puerta   : std_logic;
    signal piso         : std_logic_vector(1 downto 0);
    signal motor        : STD_LOGIC_VECTOR (1 downto 0);
    signal Puerta       : STD_LOGIC;
    
    constant clk_period : time :=100 ns;
    
    constant fmaq : positive := 1000;
    constant fpiso: positive := 500;
    constant fluz: positive := 250000;
    
begin

    uut:
     Ascensor
         Generic Map( fmaq=>fmaq,
                      fpiso=>fpiso,
                      fluz=>fluz
                     )
         Port Map(clk          => clk,
                  rst          => rst,
                  led_motor    => led_tricolor,
                  leds_serie   => leds_serie,
                  display_piso => display_piso,            
                  digctrl      => digctrl,
                  fin_puerta   => fin_puerta,
                  piso         => piso,
                  motor        => motor,
                  puerta_ON    => puerta
                 );
                 

  --Clock process definition
     clk_process: process
     begin
         CLK <= '0';
         wait for clk_period/2;
         CLK <= '1';
         wait for clk_period/2;
     end process;   
         
     --Stimulus process
     stim_proc:process
     begin  
     
 --Caso 1
 --  Paso 1: El ascensor est� en reposo y se resetea (piso 0, sin llamadas previas, motor, puertas, etc a cero�).
     --Check reset (rst='1' significa paso al estado S0)
     rst <= '0' after 0.25*clk_period;    
     wait until rst ='0';
     motor<="11";
      -- rst deassertion
      rst<='1' after 0.5*clk_period;
      wait until rst = '1';
      wait for 0.5*clk_period; 
      wait until clk ='1'AND clk'event;
      
     motor<="10";
     Puerta<='1';
      
     wait for 20*clk_period; 
     --wait for 3000 ms;    

     end process;

end Behavioral;
