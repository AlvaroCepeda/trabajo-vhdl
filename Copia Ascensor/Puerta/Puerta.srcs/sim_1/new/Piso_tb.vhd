----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.01.2020 04:21:25
-- Design Name: 
-- Module Name: Piso_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

entity Mod_piso_tb is
end Mod_piso_tb;

architecture tb of Mod_piso_tb is

   component Mod_piso is
      port( 
         clk: in std_logic;
         Rst: in std_logic;
         Motor: in std_logic_vector(0 to 1);
         suma: in std_logic;
         Piso: out std_logic_vector(0 to 1)
         );
   end component;

 
  
   signal clk : std_logic := '0';
   signal Motor : std_logic_vector(1 downto 0);
   signal Rst : std_logic ;
   signal suma: std_logic;
   signal piso : std_logic_vector(1 downto 0);

begin
   uut: Mod_piso
   port map(motor => motor, 
            clk => clk,
            Rst => Rst,  
            piso => piso, 
            suma => suma
            );

    clk <= not clk after 1 ps;

   --Concurrent processes
   process
   begin	
      rst   <= '1'; wait for 1 ps; 
      rst   <= '0';  
      motor <= "10"; 
      suma  <= '1' ; wait for 2 ps; 
      suma  <= '0' ; wait for 2 ps;
      motor <= "01"; 
      suma  <= '1' ; wait for 2 ps;
      suma  <= '0' ; wait for 2 ps;
      motor <= "00"; 
      suma  <= '1' ; wait for 2 ps;
      
     wait for 2 ps;
     ASSERT FALSE
        REPORT "End simulation"
            SEVERITY FAILURE;
   end process;

  
end tb;
