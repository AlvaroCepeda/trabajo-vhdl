----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.01.2020 14:18:57
-- Design Name: 
-- Module Name: Top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Top_tb is
--  Port ( );
end Top_tb;

architecture Behavioral of Top_tb is
    component Top is
        generic (
                    fin   : positive := 100000000;
                    fout  : positive := 1000;
                    fpiso : positive := 1;
                    fluz  : positive := 2;
                    NPISOS: positive := 4
                );
         Port (
                          clk_in         : in std_logic;
                          rst            : in std_logic;
                          boton_int      : in std_logic_vector(NPISOS-1 downto 0);
                          boton_ext      : in std_logic_vector(NPISOS-1 downto 0);
                          led_motor      : out std_logic_vector(2 downto 0);
                          LED            : out std_logic_vector (15 downto 0);
                          Display_piso   : out STD_LOGIC_VECTOR (6 downto 0);
                          digctrl        : out STD_LOGIC_VECTOR (7 downto 0)
                );
    end component Top;
    
    constant NUM_PISOS:positive:=4;
    
    signal clk,rst      : std_logic;
    signal boton_int    : std_logic_vector (Num_PISOS-1 downto 0);
    signal boton_ext    : std_logic_vector (Num_PISOS-1 downto 0);
    signal LED_motor     : std_logic_vector (2 downto 0);
    signal LEDs_serie   : std_logic_vector (15 downto 0);
    signal Display_piso : STD_LOGIC_VECTOR (6 downto 0);
    signal Digctrl      : STD_LOGIC_VECTOR (7 downto 0);
    
    constant clk_period : time :=10 ns;
begin

    uut:
    Top 
    generic map( fin  =>100000000,
                 fout => 50000000,
                 fpiso=> 25000000,
                 fluz => 25000000,
                 NPISOS=>NUM_PISOS
                )
    port map(    clk_in=>clk,
                 rst=>rst,
                 boton_int=>boton_int,
                 boton_ext=>boton_ext,
                 Display_piso=>display_piso,
                 Digctrl=>digctrl,
                 Led_motor=>Led_motor,
                 LED=>leds_serie              
                 );

 --Clock process definition
    clk_process: process
    begin
        CLK <= '0';
        wait for clk_period/2;
        CLK <= '1';
        wait for clk_period/2;
    end process;   
        

rst <= '0' after 0.25 * clk_period, '1' after 0.75 * clk_period;
--PROBAMOS BOTONES EXTERIORES E INTERIORES            
stim_proc:process
    begin
        boton_int <= "0000"; --del 0 al 1
        boton_ext <= "0010";
    wait for 38.25 * clk_period;
    --assert motor="00" and puerta= '1' 
            --report "Mal funcionamiento"
                --severity failure; 
                
    --boton_int <= "0010"; -- del 1 al 1
    --boton_ext <= "0000";
    --wait for 2 * clk_period;
    
        boton_int <= "1000"; -- del 1 al 3
        boton_ext <= "0000";
    wait for 40 * clk_period;
    --assert motor="00" and puerta= '1' 
            --report "Mal funcionamiento"
                --severity failure;
                

        boton_int <= "0000"; -- del 3 al 2
        boton_ext <= "0100";
    wait for 2 * clk_period;
    --assert motor="00" and puerta= '1' 
            --report "Mal funcionamiento"
                --severity failure;
                
        boton_int <= "0100"; -- del 2 al 2
        boton_ext <= "0000";
    wait for 2 * clk_period;
--assert motor="00" and puerta= '1' 
            --report "Mal funcionamiento"
                --severity failure;
                
        boton_int <= "0000"; -- del 2 al 0
        boton_ext <= "0001";
    wait for 2 * clk_period;
--assert motor="00" and puerta= '1' 
            --report "Mal funcionamiento"
                --severity failure;
                
     end process;
     end architecture;