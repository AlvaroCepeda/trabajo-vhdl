----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.01.2019 18:21:19
-- Design Name: 
-- Module Name: Top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Top_tb is
--  Port ( );
end Top_tb;

architecture Behavioral of Top_tb is
    component Top is
    Port (
            CLK : in std_logic;
            RST: IN std_logic;
            BOTON: IN std_logic_vector(3 downto 0);
            MOTOR_PUERTA, MOTOR_ASCENSOR: OUT std_logic_vector(1 downto 0);
            SENSOR, DESTINO: OUT std_logic_vector(6 downto 0)
            );
    end component Top;
    
    signal CLK :  std_logic;
    signal RST:  std_logic;      
    signal BOTON:  std_logic_vector(3 downto 0);
    signal MOTOR_PUERTA, MOTOR_ASCENSOR:  std_logic_vector(1 downto 0);
    signal SENSOR, DESTINO:  std_logic_vector(6 downto 0);
    
    constant clk_period: time :=10ns;
        
    

begin
    
    uut: Top
    port map(CLK=>CLK,
            RST=>RST,
            BOTON=>BOTON,
            MOTOR_PUERTA=>MOTOR_PUERTA,
            MOTOR_ASCENSOR=>MOTOR_ASCENSOR,
            SENSOR=>SENSOR,
            DESTINO=>DESTINO
            );
            
     --Clock process definition
     clk_process: process
     begin
            CLK<= '0';
            wait for clk_period/2;
            CLK<= '1';
            wait for clk_period/2;
     end process;
     
     
     --stimulus process
     stim_proc:process
     begin
     
     --Caso 1
     --Ascensor en reposo y se resetea
     RST<='0' after 0.25*clk_period;
     wait until RST= '0';
     
     rst<='1' after 0.5*clk_period;
     wait until RST<='1';
     wait for 0.5*clk_period;
     wait until CLK='1' AND clk'event;
     
     BOTON<= "0000";
     
     --Desde dentro del ascensor se poide que suba al primer piso
     BOTON<= "0001";
     
     wait for 7*clk_period;
     wait for 20000 ms;
     assert false
        report "Simulation complete"
            severity failure;
     end process;
     

end Behavioral;
