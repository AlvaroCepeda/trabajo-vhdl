----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.11.2019 13:52:31
-- Design Name: 
-- Module Name: M_estados_tb3 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity M_estados_tb3 is
--  Port ( );
end M_estados_tb3;

architecture Behavioral of M_estados_tb3 is

    Component M_estados is
     Generic( num_pisos:natural:=4);
     
     Port (clk,reset    : in STD_LOGIC;
           boton_int    : in STD_LOGIC_VECTOR(num_pisos-1 downto 0);
           boton_ext    : in STD_LOGIC_VECTOR(num_pisos-1 downto 0);
           piso         : out STD_LOGIC_VECTOR(1 downto 0);
           motor        : out STD_LOGIC_VECTOR (1 downto 0);
           puerta       : out STD_LOGIC );
    end Component M_estados;
    
    signal clk,reset  : STD_LOGIC;
    signal boton_int  : STD_LOGIC_VECTOR (3 downto 0);
    signal boton_ext  : STD_LOGIC_VECTOR (3 downto 0);
    signal piso       : STD_LOGIC_VECTOR (1 downto 0);
    signal motor      : STD_LOGIC_VECTOR (1 downto 0);
    signal puerta     : STD_LOGIC;
    
    constant clk_period : time :=100 ns;

begin

uut: M_estados 
         generic map( num_pisos=>4 )
         
         port map ( clk=>clk,
                    reset=>reset,
                    boton_int=>boton_int,
                    boton_ext=>boton_ext,
                    piso=>piso,
                    motor=>motor,
                    puerta=>puerta
                   );
                   
   --Clock process
clk_process: process
    begin
        clk <= '0';
        wait for 0.5 * clk_period;
        clk <= '1';
        wait for 0.5 * clk_period;
    end process;
  
  reset <= '1' after 0.25 * clk_period, '0' after 0.75 * clk_period;
--PROBAMOS BOTONES EXTERIORES E INTERIORES            
stim_proc:process
    begin
        boton_int <= "0000"; --del 0 al 1
        boton_ext <= "0010";
    wait for 3.25 * clk_period;
    assert motor="00" and puerta= '1' 
            report "Mal funcionamiento"
                severity failure; 
                
    --boton_int <= "0010"; -- del 1 al 1
    --boton_ext <= "0000";
    --wait for 2 * clk_period;
    
        boton_int <= "1000"; -- del 1 al 3
        boton_ext <= "0000";
    wait for 2 * clk_period;
    assert motor="00" and puerta= '1' 
            report "Mal funcionamiento"
                severity failure;
                

        boton_int <= "0000"; -- del 3 al 2
        boton_ext <= "0100";
    wait for 2 * clk_period;
    assert motor="00" and puerta= '1' 
            report "Mal funcionamiento"
                severity failure;
                
        boton_int <= "0100"; -- del 2 al 2
        boton_ext <= "0000";
    wait for 2 * clk_period;
    assert motor="00" and puerta= '1' 
            report "Mal funcionamiento"
                severity failure;
                
        boton_int <= "0000"; -- del 2 al 0
        boton_ext <= "0001";
    wait for 2 * clk_period;
    assert motor="00" and puerta= '1' 
            report "Mal funcionamiento"
                severity failure;
                
     end process;
end Behavioral;
