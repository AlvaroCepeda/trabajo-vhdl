library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity cerrar_puerta_tb is
end cerrar_puerta_tb;

architecture tb of cerrar_puerta_tb is

   component cerrar_puerta is
      port(  
       cierre_de_puertas: in std_logic_vector(2 downto 0);
       out_decod: out std_logic_vector(15 downto 0)
            );-- the output
   end component;

 
   signal out_decod : std_logic_vector(15 downto 0);
   signal puertas : std_logic_vector(2 downto 0);
  

begin
   uut: cerrar_puerta
   port map(out_decod => out_decod,
            cierre_de_puertas => puertas 
            );

   --Concurrent processes
   process
   begin	
      puertas   <= "000"; wait for 1 ns;
      puertas   <= "001"; wait for 1 ns; 
      puertas   <= "010"; wait for 1 ns; 
      puertas   <= "011"; wait for 1 ns; 
      puertas   <= "100"; wait for 1 ns; 
      puertas   <= "101"; wait for 1 ns; 
      puertas   <= "110"; wait for 1 ns;  
      puertas   <= "111"; wait for 1 ns; 
   end process;

  
end tb;
