----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.11.2019 02:18:42
-- Design Name: 
-- Module Name: LedMotor_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LedMotor_tb is
--  Port ( );
end LedMotor_tb;

architecture Behavioral of LedMotor_tb is

component LedMotor is
        Port(
        motor: in std_logic_vector(1 downto 0);
        Led_Motor: out std_logic_vector(2 downto 0)
         );
end component;

signal motor: std_logic_vector(1 downto 0);
signal Led_Motor: std_logic_vector(2 downto 0);

begin
   uut: LedMotor 
   port map(
            motor => motor, 
            Led_Motor => Led_Motor
            );
            
process
   begin	
   motor <= "00"; 
   wait for 100 ns; 
   motor <= "01"; 
   wait for 100 ns;  
   motor <= "10"; 
   wait for 100 ns;
   motor <= "11"; 
   wait for 100 ns;
end process;

end Behavioral;
