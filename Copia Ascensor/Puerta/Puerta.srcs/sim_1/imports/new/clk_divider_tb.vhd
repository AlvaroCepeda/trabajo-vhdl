
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
 
ENTITY clk_divider_tb IS
END clk_divider_tb;
 
ARCHITECTURE behavior OF clk_divider_tb IS 
 
    COMPONENT clk_divider
    GENERIC (freq_in: positive;
             freq_out: positive);
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         clk_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk     : std_logic := '0';
   signal reset : std_logic := '0';

 	--Outputs
   signal clk_out : std_logic;
   signal clk_out_tb : std_logic;
   -- Clock period definitions
   constant clk_period : time := 10 ns;
   constant clk_out_period : time := 1000 ms;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: clk_divider 
            GENERIC MAP(
                        freq_in=>100000000,
                        freq_out=>1
                        )
                        
            PORT MAP (
                      clk     => clk,
                      reset => reset,
                      clk_out => clk_out
                     );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   clk_out_process :process
    begin
		clk_out_tb <= '0';
		wait for clk_out_period/2;
		clk_out_tb <= '1';
		wait for clk_out_period/2;
     end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      reset<='0';
		wait for 50 ns;
		
		reset<='1';
		wait for 50 ns;
		
		reset<='0';
	
      wait for 1 ms;
      ASSERT clk_out=clk_out_tb
        REPORT "Clk_divider malfunction"
            SEVERITY FAILURE; 

      wait for 3 ms;
      ASSERT FALSE
        REPORT "End simulation"
            SEVERITY FAILURE;
   end process;

END;