library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity puerta_tb is
end puerta_tb;

architecture tb of puerta_tb is

   component puertacomponent is
      port(  
      puerta_ON : in std_logic;
      clk : in std_logic;
      Rst: in std_logic;
      Fin_puerta : out std_logic;
      puertas : out std_logic_vector(2 downto 0)
            );
   end component;

 
   signal puerta_ON: std_logic;
   signal clk : std_logic := '0';
   signal Fin_puerta : std_logic;
   signal Rst : std_logic;
   signal puertas : std_logic_vector(2 downto 0);
  

begin
   uut: puertacomponent 
   port map(puerta_ON => puerta_ON, 
            clk => clk,
            Rst => Rst, 
            Fin_puerta => Fin_puerta, 
            puertas => puertas 
            );


       clk <= not clk after 1 ps;
   process
   begin	

       
      rst       <= '0' after 1 ps; wait for 2 ps; 
      rst       <= '1';  
      puerta_ON <= '0'; wait for 1 ps;
      puerta_ON <= '1'; wait for 10 ps;
      puerta_ON <= '0';  wait for 1 ps;
      puerta_ON <= '1'; wait for 20 ps;
      
      --assert false
        --report "End simulation"
           --severity failure;
   end process;

  
end tb;
